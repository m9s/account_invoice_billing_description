#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import datetime
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Bool, Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


_STATES = {
    'readonly': Bool(Eval('invoice_line')),
}
_DEPENDS = ['invoice_line']


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    description = fields.Text('Description', states=_STATES, depends=_DEPENDS)

    def __init__(self):
        super(BillingLine, self).__init__()
        self.product = copy.copy(self.product)
        if not self.product.on_change:
            self.product.on_change = []
        if 'billing_date' not in self.product.on_change:
            self.product.on_change += ['billing_date']
        if 'party' not in self.product.on_change:
            self.product.on_change += ['party']
        self.billing_date = copy.copy(self.billing_date)
        if not self.billing_date.on_change:
            self.billing_date.on_change = []
        for field in ['billing_date', 'product', 'party']:
            if field not in self.billing_date.on_change:
                self.billing_date.on_change += [field]
        self._reset_columns()
        self._rpc.update({
                          'on_change_billing_date': False,
                          })

    def on_change_product(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        party_obj = pool.get('party.party')
        lang_obj = pool.get('ir.lang')
        user_obj = pool.get('res.user')

        if not vals.get('product'):
            return {}
        res = super(BillingLine, self).on_change_product(vals)
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            lang_id = party.lang.id
        if not vals.get('party') or lang_id==False:
            user = user_obj.browse(Transaction().user)
            lang_id = user.language.id
        lang = lang_obj.browse(lang_id)

        product = product_obj.browse(vals['product'])
        if vals.get('billing_date'):
            res['description'] = vals['billing_date'].strftime(str(lang.date)) + \
                    " " + product.name
        else:
            res['description'] = product.name
        return res

    def on_change_billing_date(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        party_obj = pool.get('party.party')
        lang_obj = pool.get('ir.lang')
        user_obj = pool.get('res.user')
        res = {}
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            lang_id = party.lang.id
        if not vals.get('party') or lang_id==False:
            user = user_obj.browse(Transaction().user)
            lang_id = user.language.id
        lang = lang_obj.browse(lang_id)

        if vals.get('product'):
            product = product_obj.browse(vals['product'])
            if vals.get('billing_date'):
                res['description'] = vals['billing_date'].strftime(str(lang.date)) + \
                        " " + product.name
            else:
                res['description'] = product.name
        return res


    def _get_invoice_line_billing(self, line, invoice_id, accounting_date):
        res = super(BillingLine, self)._get_invoice_line_billing(line,
                invoice_id, accounting_date)
        res['description'] = line.description or line.product.name
        return res

BillingLine()
