#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Billing Description',
    'name_de_DE': 'Fakturierung Rechnungsstellung aus Abrechnungsdaten mit Beschreibung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Billing Description
    - Adds a date and comment field to Account Invoice Billing.
''',
    'description_de_DE': '''Fakturierung Rechnungsstellung aus Abrechnungsdaten mit individueller Beschreibung
    - Erweitert die Generierung von Rechnungen auf Basis von in einer
      Tabelle erfassten Abrechnungsdaten um ein Datums- und ein
      Kommentarfeld.
''',
    'depends': [
        'account_invoice_billing',
    ],
    'xml': [
        'billing.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
